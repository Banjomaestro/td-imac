#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex)
{
    
	return (nodeIndex*2)+1;
}

int Heap::rightChild(int nodeIndex)
{
    return (nodeIndex*2)+2;;
}



void Heap::insertHeapNode(int heapSize, int value)
{

    int i = heapSize;
    this->get(i)=value;
    while(i>0 && this->get(i) > this->get((i-1)/2)){
        this->swap(i,(i-1)/2);
        i=(i-1)/2;
    }
    
}

void Heap::heapify(int heapSize, int nodeIndex)
{
	int largest = nodeIndex;
	

	int l = leftChild(nodeIndex);
	int r = rightChild(nodeIndex);
	
	if(l<heapSize){
		int left = this->get(leftChild(nodeIndex));
	if ( left > this->get(largest))
        largest = l;
	}
  
    // If right child is larger thaheapSize largest so far
    if(r<heapSize){
		int right = this->get(rightChild(nodeIndex));
	if ( right > this->get(largest))
        largest = r;
	}
	if(largest!=nodeIndex){
		this->swap(nodeIndex,largest);
		this->heapify(heapSize,largest);
	}
}

void Heap::buildHeap(Array& numbers)
{
	int size = numbers.size();
	for(int i=0; i<size;i++){
		this->insertHeapNode(this->size(),numbers.get(i));
	}

}

void Heap::heapSort()
{
	int size = this->size();
	for(int i=size-1;i>=0;i--){
		swap(this->get(0),this->get(i));
		heapify(i,0);
	}

}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
	w->show();

	return a.exec();
}

