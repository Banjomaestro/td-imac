#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>

MainWindow* w = nullptr;
using std::size_t;

struct BinarySearchTree : public BinaryTree
{    
    Node* left;
    Node* right;
    int value;

    void initNode(int value)
    {
        this->value = value;
        left = NULL;
        right = NULL;
        // init initial node without children
    }

	void insertNumber(int value) {
        if(value>this->value){
            if(right != NULL)
                right->insertNumber(value);
            else
                right = new BinarySearchTree(value);
        }
        if(value<this->value){
            if(left != NULL)
                left->insertNumber(value);
            else
                left = new BinarySearchTree(value);
        }
    }

	uint height() const	{
        
        int righteroo = 0;
        int lefteroo = 0;
        
        if(right != NULL)
            righteroo = right->height()+1;
        else 
            righteroo = 1;
        
        if(left != NULL)
            lefteroo = left->height()+1;
        else 
            lefteroo =  1;

        if(righteroo>lefteroo)
            return righteroo;
        else 
            return lefteroo;
    }

	uint nodesCount() const {
        
        int righteroo = 0;
        int lefteroo = 0;
        
        if(right != NULL)
            righteroo = right->nodesCount();
        
        if(left != NULL)
            lefteroo = left->nodesCount();



        return (righteroo+lefteroo+1);
	}

	bool isLeaf() const {
        
        if(right == NULL && left == NULL){
            return true;
        }else 
            return false;
	}

	void allLeaves(Node* leaves[], uint& leavesCount) {

        leaves[leavesCount] = this;
        leavesCount++;
        if(right!=NULL)
            right->allLeaves(leaves, leavesCount);
        if(left!=NULL)
            left->allLeaves(leaves, leavesCount); 
        
        
	}

	void inorderTravel(Node* leaves[], uint& leavesCount) {

        if(left!=NULL){
            left->inorderTravel(leaves,leavesCount);
        }
        leaves[leavesCount] = this;
        leavesCount++;
        if(right!=NULL){
            right->inorderTravel(leaves,leavesCount);
        }
        // fill nodes array with all nodes with inorder travel
	}

	void preorderTravel(Node* leaves[], uint& leavesCount) {

        leaves[leavesCount] = this;
        leavesCount++;
        if(left!=NULL){
            left->preorderTravel(leaves,leavesCount);
        }
        if(right!=NULL){
            right->preorderTravel(leaves,leavesCount);
        }
        
        
        
        // fill nodes array with all nodes with preorder travel
	}

	void postorderTravel(Node* nodes[], uint& nodesCount) {
        if(left!=NULL){
            left->postorderTravel(nodes,nodesCount);
        }
        if(right!=NULL){
            right->postorderTravel(nodes,nodesCount);
        }
        
        nodes[nodesCount] = this;
        nodesCount++;
        // fill nodes array with all nodes with postorder travel
	}

	Node* find(int value) {
        if(this->value == value)
            return this;
        else if(right != NULL && value>this->value)
            return right->find(value);
        else if(left != NULL && value<this->value)
            return left->find(value);
		return nullptr;
	}

    void reset()
    {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    BinarySearchTree(int value) : BinaryTree(value) {
        initNode(value);
        }
    ~BinarySearchTree() {}
    int get_value() const {return value;}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

Node* createNode(int value) {
    return new BinarySearchTree(value);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<BinarySearchTree>();
	w->show();

	return a.exec();
}
