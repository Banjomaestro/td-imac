#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;

bool arraySortedOrNot(Array& sorted)
{
    for(int i = 1; i<sorted.size(); i++){
    if (sorted.get(i) < sorted.get(i-1))
        return false;
	}
	return true;
}


void bubbleSort(Array& toSort){

	while(!arraySortedOrNot(toSort)){
	for(int i = 0; i<toSort.size()-1; i++){
		if(toSort.get(i+1)<toSort.get(i)){
			toSort.swap(i+1,i);
		}
	}
	}
}



int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	uint elementCount=20;
	MainWindow::instruction_duration = 100;
	w = new TestMainWindow(bubbleSort);
	w->show();

	return a.exec();
}
