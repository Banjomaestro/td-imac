#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;

void insertionSort(Array& toSort){
	
	Array& sorted=w->newArray(toSort.size());
	int ysize = 0;
	

	for(int i = 0; i < toSort.size() ; i++){
		int toInsert = toSort.get(i);
		bool inserted = false;

		for(int y = 0 ; y < ysize; y++){

			if(toInsert<sorted.get(y)){
				sorted.insert(y,toInsert);
				inserted = true;
				break;
				}
			
			}
			
			if(!inserted){
				sorted.insert(ysize,toInsert);
			}
			ysize++;
		} 
	toSort = sorted;
	 // update the original array
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(insertionSort); // window which display the behavior of the sort algorithm
	w->show();

	return a.exec();
}
