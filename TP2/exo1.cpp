#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w = nullptr;

void selectionSort(Array& toSort){
    
    
    int indexmin = 0;

	for(int i = 0; i <toSort.size(); i++){
    int minimum = toSort.get(i);    
    indexmin = i;
        for(int y = i; y <toSort.size(); y++){

        if(toSort.get(y)<minimum){
            minimum = toSort.get(y);
            indexmin = y;
        }
        }
        toSort.swap(i,indexmin);
    }
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(selectionSort); // window which display the behavior of the sort algorithm
    w->show();

	return a.exec();
}
