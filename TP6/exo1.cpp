#include "tp6.h"
#include <QApplication>
#include <time.h>

MainWindow* w = nullptr;

void Graph::buildFromAdjenciesMatrix(int **adjacencies, int nodeCount)
{
	/**
	  * Make a graph from a matrix
	  * first create all nodes, add it to the graph then connect them
	  * this->appendNewNode
	  * this->nodes[i]->appendNewEdge
	  */

	 for(int i =0; i < nodeCount; i++){
		 GraphNode* newOne = new GraphNode(i);
		 this->appendNewNode(newOne);
	 }

	 for(int i =0; i<nodeCount;i++){
		 for(int k =0; k<nodeCount;k++){
			 if(adjacencies[i][k]>=1){
				 this->nodes[i]->appendNewEdge(nodes[k],adjacencies[i][k]);
			 }

		 
	 	}

	 }
}

void Graph::deepTravel(GraphNode *first, GraphNode *nodes[], int &nodesSize, bool visited[])
{
	/**
	  * Fill nodes array by travelling graph starting from first and using recursivity
	  */
	 if(visited[first->value])
	 return;

	nodes[nodesSize] = first;
	nodesSize++;
	visited[first->value] = true;

    for(Edge* e=first->edges; e != NULL; e=e->next){
        deepTravel(e->destination,nodes,nodesSize,visited);
	}
}

void Graph::wideTravel(GraphNode *first, GraphNode *nodes[], int &nodesSize, bool visited[])
{
	/**
	 * Fill nodes array by travelling graph starting from first and using queue
	 * nodeQueue.push(a_node)
	 * nodeQueue.front() -> first node of the queue
	 * nodeQueue.pop() -> remove first node of the queue
	 * nodeQueue.size() -> size of the queue
	 */
	std::queue<GraphNode*> nodeQueue;
	nodeQueue.push(first);
	while(!nodeQueue.empty()){
		nodes[nodesSize] = nodeQueue.front();
		nodeQueue.pop();
		nodesSize++;
		visited[first->value] = true;
		for(Edge* e=first->edges; e != NULL; e=e->next){
			if(!visited[e->destination->value])
				nodeQueue.push(e->destination);
		}
	}
}

bool Graph::detectCycle(GraphNode *first, bool visited[])
{
	/**
	  Detect if there is cycle when starting from first
	  (the first may not be in the cycle)
	  Think about what's happen when you get an already visited node
	**/	

	visited[first->value] = true;
	if(visited[first->value]){
		return true;
	}
	for(Edge* e=first->edges; e != NULL; e=e->next){
    	return detectCycle()||false;
	}
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 150;
	w = new GraphWindow();
	w->show();

	return a.exec();
}
