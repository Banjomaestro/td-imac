#include "tp1.h"
#include <QApplication>
#include <time.h>

int isMandelbrot(Point z, int n, Point point){
    // recursiv Mandelbrot

    if (n == 0)
        return 0;
    else if ((z.x*z.x+z.y*z.y) > 2*2)
        return n;
    else {
        float temporary = z.x;
        z.x = point.x + z.x*z.x - z.y*z.y;
        z.y = point.y + 2*temporary*z.y;
        return isMandelbrot(z, --n, point);
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow* w = new MandelbrotWindow(isMandelbrot);
    w->show();

    a.exec();
}



