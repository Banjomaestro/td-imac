#include <iostream>

using namespace std;

struct Noeud{
    int data;
    Noeud* next = nullptr;

};

struct Liste{
    
    Noeud* first = nullptr;
};

struct DynaTableau{
    int* datas = nullptr;
    size_t size = 0;
    size_t capacity = 0;
};


void initialize(Liste* liste){
    liste->first = nullptr;
}

bool isEmpty(const Liste* liste){
    return !(liste->first);
}

void add(Liste* liste, int valeur){
    Noeud* temp;
    if (isEmpty(liste)) {
        liste->first = new Noeud;
        temp = liste->first;
    }
    else {
        temp = liste->first;
        while (temp->next != nullptr) {
            temp = temp->next;
        }
        temp->next = new Noeud;
        temp = temp->next;
    }
    temp->data = valeur;
}

void affiche(const Liste* liste){
    if (isEmpty(liste)) {
        std::cout << "La liste est vide" << std::endl;
        return;
    }
    Noeud* temp = liste->first;
    while (temp->next != nullptr) {
        std::cout << temp->data << std::endl;
        temp = temp->next;
    }
    std::cout << temp->data << std::endl;
}

int recupere(const Liste* liste, int n){
    if (isEmpty(liste)) {
        std::cout << "La liste est vide" << std::endl;
        return 0;
    }
    Noeud* temp = liste->first;
    int i;
    for (i = 0; i < n && temp->next != nullptr; i++) {
        temp = temp->next;
    }
    if (i == n) return temp->data;
    std::cout << "\nLe " << n << "ieme élément n'existe pas." << std::endl;
    return 0;
}

int cherche(const Liste* liste, int valeur){
    if (isEmpty(liste))
        return -1;
    Noeud* temp = liste->first;
    int i;
    for (i = 0; temp->next != nullptr && temp->data != valeur; i++) {
        temp = temp->next;
    }
    if (temp->data == valeur) return i;
    return -1;
}

void stocke(Liste* liste, int n, int valeur){
    if (isEmpty(liste))
        return;
    Noeud* temp = liste->first;
    int i;
    for (i = 1; temp->next != nullptr && i < n; i++) {
        temp = temp->next;
    }
    if (i == n) temp->data = valeur;
}

void add(DynaTableau* tableau, int valeur){
    if (tableau->size == tableau->capacity) {
        tableau->capacity *= 2;
        int* temp = new int[tableau->capacity];
        for (size_t i = 0; i < tableau->size ; i++) {
            temp[i] = tableau->datas[i];
        }
        delete [] tableau->datas;
        tableau->datas = temp;
    }
    tableau->datas[tableau->size++] = valeur;
}


void initialize(DynaTableau* tableau, int capacity){
    tableau->capacity = capacity;
    tableau->datas = new int[capacity];
}

bool isEmpty(const DynaTableau* tableau){
    return (tableau->size == 0);
}

void affiche(const DynaTableau* tableau){
    if (isEmpty(tableau)) {
        std::cout << "Le tableau est vide." << std::endl;
        return;
    }
    for (size_t i = 0; i < tableau->size; i++) {
        std::cout << tableau->datas[i] << std::endl;
    }
}

int recupere(const DynaTableau* tableau, int n){
    if ((int)n-1 >= tableau->size) {
        std::cout << "Le " << n << "ième élement n'existe pas." << std::endl;
        return 0;
    }
    return tableau->datas[n];
}

int cherche(const DynaTableau* tableau, int valeur){
    size_t i;
    for (i = 0; i < tableau->size  ; i++) {
        if (tableau->datas[i] == valeur) {
            return i;
        }
    }
    std::cout << "La valeur n'existe pas dans le tableau." << std::endl;
    return 0;
}

void stocke(DynaTableau* tableau, int n, int valeur){
    if ((int)n >= tableau->size) {
        std::cout << "Le " << n << "ième élement n'existe pas." << std::endl;
        return;
    }
    tableau->datas[n] = valeur;
}


void pousse_file(Liste* liste, int valeur){
    add(liste, valeur);
}


int retire_file(Liste* liste){
    if (isEmpty(liste)) {
        std::cout << "File vide." << std::endl;
        return 0;
    }

    Noeud* temp = liste->first->next;
    int tempI = liste->first->data;
    delete liste->first;
    liste->first = temp;
    return tempI;
}

void pousse_pile(DynaTableau* liste, int valeur){
    add(liste, valeur);
}

int retire_pile(DynaTableau* liste){
    if (isEmpty(liste)) {
        std::cout << "Pile vide." << std::endl;
        return 0;
    }
    return liste->datas[--liste->size];
}


int main(){
    Liste liste;
    initialize(&liste);
    DynaTableau tableau;
    initialize(&tableau, 5);

    if (!isEmpty(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!isEmpty(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        add(&liste, i*7);
        add(&tableau, i*5);
    }

    if (isEmpty(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (isEmpty(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    
    DynaTableau pile;
    Liste file; 

    initialize(&pile, 10);
    initialize(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!isEmpty(&file) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 10;
    while(!isEmpty(&pile) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}
